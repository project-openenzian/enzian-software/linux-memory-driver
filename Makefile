obj-m += enzian_memory.o

all: emd mb_enzian mem

emd:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules

mb_enzian: mb_enzian.c
	gcc -o mb_enzian mb_enzian.c -Wall -O2 -lpthread -mtune=thunderx

mem: mem.c
	gcc -o mem mem.c -Wall -O2

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
	rm -f mb_enzian mem
